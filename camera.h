#ifndef CAMERA_H
#define CAMERA_H

#include <glad/glad.h>
#include <GLFW/glfw3.h>

#include <glm/glm.hpp>
#include<glm/gtc/matrix_transform.hpp>

// Defines several possible options for camera movement. Used as abstraction to stay away from window-system specific input methods
enum Camera_Movement {
    FORWARD,
    BACKWARD,
    LEFT,
    RIGHT
};

// Default camera values
const float YAW = -90.0f;
const float PITCH = 0.0f;
const float SPEED = 2.5f;
const float SENSITIVITY = 0.1f;
const float ZOOM = 45.0f;


// An abstract camera class that processes input and calculates the corresponding Euler Angles, Vectors and Matrices for use in OpenGL
class Camera
{
public:
    // camera Attributes
    glm::vec3 Position;
    glm::vec3 Front;
    glm::vec3 Up;
    glm::vec3 Right;
    glm::vec3 WorldUp;
    bool firstClick = true;
    bool up_b = false;
    float timer = 0;


    double now1, now2, now3;




    // euler Angles
    float Yaw;
    float Pitch;
    // camera options
    float MovementSpeed;
    float MouseSensitivity;
    float Zoom;
    const int width = 500;
    const int height = 500;

    // constructor with vectors
    Camera(glm::vec3 position = glm::vec3(0.0f, 0.0f, 0.0f), glm::vec3 up = glm::vec3(0.0f, 1.0f, 0.0f), float yaw = YAW, float pitch = PITCH) : Front(glm::vec3(0.0f, 0.0f, -1.0f)), MovementSpeed(SPEED), MouseSensitivity(SENSITIVITY), Zoom(ZOOM)
    {
        this->Position = position;
        this->WorldUp = up;
        this->Yaw = yaw;
        this->Pitch = pitch;
        this->updateCameraVectors();
    }
    // constructor with scalar values
    Camera(float posX, float posY, float posZ, float upX, float upY, float upZ, float yaw, float pitch) : Front(glm::vec3(0.0f, 0.0f, -1.0f)), MovementSpeed(SPEED), MouseSensitivity(SENSITIVITY), Zoom(ZOOM)
    {
        this->Position = glm::vec3(posX, posY, posZ);
        this->WorldUp = glm::vec3(upX, upY, upZ);
        this->Yaw = yaw;
        this->Pitch = pitch;
        this->updateCameraVectors();
    }

    // returns the view matrix calculated using Euler Angles and the LookAt Matrix
    glm::mat4 GetViewMatrix()
    {
        return glm::lookAt(this->Position, this->Position + this->Front, this->Up);
    }

    glm::mat4 GetProjectionMatrix(float fov = 45.0, float ratio = 1.0, float near = 0.01, float far = 100.0)
    {
        return glm::perspective(fov, ratio, near, far);
    }

    // processes input received from any keyboard-like input system. Accepts input parameter in the form of camera defined ENUM (to abstract it from windowing systems)
    void ProcessKeyboardMovement(Camera_Movement direction, float deltaTime)
    {

        ProcessKeyboardRotation(0.0, -this->Pitch, 0.01);

        float velocity = this->MovementSpeed * deltaTime;
        if (direction == FORWARD && isItOkay(this->Front, velocity))
            this->Position += this->Front * velocity;
        if (direction == BACKWARD && isItOkay(-this->Front, velocity))
            this->Position -= this->Front * velocity;
        if (direction == LEFT && isItOkay(-this->Right, velocity))
            this->Position -= this->Right * velocity;
        if (direction == RIGHT && isItOkay(this->Right, velocity))
            this->Position += this->Right * velocity;
    }

    void ProcessKeyboardRotation(float YawRot, float PitchRot, float deltaTime, GLboolean constrainPitch = true)
    {
        float velocity = this->MovementSpeed * deltaTime;
        YawRot *= velocity;
        PitchRot *= velocity;

        this->Yaw += YawRot;
        this->Pitch += PitchRot;


        // Make sure that when pitch is out of bounds, screen doesn't get flipped
        if (constrainPitch)
        {
            if (this->Pitch > 89.0f)
                this->Pitch = 89.0f;
            if (this->Pitch < -89.0f)
                this->Pitch = -89.0f;
        }
        updateCameraVectors();

    }

    // processes input received from a mouse input system. Expects the offset value in both the x and y direction.
    void ProcessMouseMovement(float xoffset, float yoffset, GLFWwindow* window, GLboolean constrainPitch = true)
    {
        if (firstClick) {
            glfwSetCursorPos(window, width / 2, width / 2);
            firstClick = false;
        }
        else {
            xoffset *= 0.2;
            yoffset *= 0.2;

            this->Yaw += xoffset;
            this->Pitch -= yoffset;

            // Make sure that when pitch is out of bounds, screen doesn't get flipped
            if (constrainPitch)
            {
                if (this->Pitch > 89.0f)
                    this->Pitch = 89.0f;
                if (this->Pitch < -89.0f)
                    this->Pitch = -89.0f;
            }

            // Update Front, Right and Up Vectors using the updated Eular angles
            this->updateCameraVectors();

        }


    }

    // processes input received from a mouse scroll-wheel event. Only requires input on the vertical wheel-axis
    void ProcessMouseScroll(float yoffset)
    {
        Zoom -= (float)yoffset;
        if (Zoom < 1.0f)
            Zoom = 1.0f;
        if (Zoom > 45.0f)
            Zoom = 45.0f;
    }

    void setfirstClick(bool click)
    {
        firstClick = click;


    }

private:
    // calculates the front vector from the Camera's (updated) Euler Angles
    void updateCameraVectors()
    {
        // calculate the new Front vector
        glm::vec3 front;
        front.x = cos(glm::radians(Yaw)) * cos(glm::radians(Pitch));
        front.y = 0.0;//sin(glm::radians(Pitch));
        front.z = sin(glm::radians(Yaw)) * cos(glm::radians(Pitch));
        Front = glm::normalize(front);
        // also re-calculate the Right and Up vector
        Right = glm::normalize(glm::cross(Front, WorldUp));  // normalize the vectors, because their length gets closer to 0 the more you look up or down which results in slower movement.
        Up = glm::normalize(glm::cross(Right, Front));
    }


    bool isItOkay(glm::vec3 dir, float velocity)
    {


        glm::vec3 pos = this->Position;
        glm::vec3 curPos = this->Position;
        pos += dir * velocity;
        float thick = 0.2;


        float border[]{
            10.0,  10.0,  // z axis
            10.0, -10.0,  // x axis
           -10.0, -10.0,  // z axis
           -10.0,  10.0,  // x axis
        };

        float insideBorder1[]{
            6.0, 10.0,
            6,-6,
            4,-6,
        };
        float insideBorder2[]{
            0, -6.0,
            -6,-6
            - 6,-10
        };
        // test for outside border

        if (pos.x >= (10.0 - thick) or pos.x <= (-10.0 + thick))
            return false;
        else if (pos.z >= (10.0 - 3 * thick) or pos.z <= (-10.0 + thick))
            return false;

        //test for hallway

        if (curPos.x > 6.0 && curPos.z > -6.0) {
            if (pos.x < (6 + thick))
                return false;
        }
        if (curPos.x <-6.0 && curPos.z > -(6.0 + thick)) {
            if (pos.x > -(6 + thick))
                return false;
        }
        if (curPos.z < -(6.0 + thick) && (curPos.x <(0 + thick) or curPos.x>(4 - thick))) { // door
            if (curPos.x > -(6 + thick) && curPos.x < 6 + thick) {
                if (pos.z > -(6 + thick))
                    return false;
            }
        }


        // test for inside room

        if (curPos.x > -(6 - thick) && curPos.x < (6 - thick) && curPos.z> -(6 - thick)) {
            if (pos.x < -(6 - 4 * thick) or pos.x >(6 - 4 * thick))
                return false;
            if (pos.z < -(6 - 4 * thick)) {
                if (curPos.x > (0 + thick) && curPos.x < (4 - thick)) // door
                    return true;
                else return false;
            }



        }








        else
            return true;



    }





};

#endif