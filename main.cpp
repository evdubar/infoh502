#include<iostream>

//include glad before GLFW to avoid header conflict or define "#define GLFW_INCLUDE_NONE"
#include <glad/glad.h>
#include <GLFW/glfw3.h>

#include <glm/glm.hpp>
#include<glm/gtc/matrix_transform.hpp>
#include<glm/gtc/type_ptr.hpp>


#define STB_IMAGE_IMPLEMENTATION
#include "stb/stb_image.h"
#include <math.h>

#include "camera.h"
#include "shader.h"
#include "object.h"

#include<map>

const int width = 2160;
const int height = 2160;

void renderCube();
void renderQuad();
//GLuint compileShader(std::string shaderCode, GLenum shaderType);
//GLuint compileProgram(GLuint vertexShader, GLuint fragmentShader);
void loadCubemapFace(const char* path, const GLenum& targetFace);
void processInput(GLFWwindow* window);
unsigned int loadTexture(char const* path);


Camera camera(glm::vec3(7.8, 2.5, 9.0));


float randf()
{
	return -1.0f + (rand() / (RAND_MAX / 2.0f));
}

int main(int argc, char* argv[])
{
	std::cout << "Welcome to exercice 7: " << std::endl;
	std::cout << "Complete light equation and attenuation factor\n"
		"Implement the complete light equation.\n"
		"Make the light move with time closer and further of the model.\n"
		"Put the attenuation factor into the calculations\n"
		"\n";


	//Boilerplate
	//Create the OpenGL context 
	if (!glfwInit()) {
		throw std::runtime_error("Failed to initialise GLFW \n");
	}
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 0);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

	#ifndef NDEBUG
	//create a debug context to help with Debugging
	glfwWindowHint(GLFW_OPENGL_DEBUG_CONTEXT, true);
	#endif


	//Create the window
	GLFWwindow* window = glfwCreateWindow(width, height, "Lost", nullptr, nullptr);
	if (window == NULL)
	{
		glfwTerminate();
		throw std::runtime_error("Failed to create GLFW window\n");
	}

	glfwMakeContextCurrent(window);

	//load openGL function
	if (!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress))
	{
		throw std::runtime_error("Failed to initialize GLAD");
	}

	glEnable(GL_DEPTH_TEST);




	char pathCube[] = "objets/cube.obj";





	Shader pbrShader("pbr.vs", "pbr.fs");
	Shader irradianceShader("cubemap.vs", "irrConv.fs");
	Shader prefilterShader("cubemap.vs", "prefilter.fs");
	Shader brdfShader("brdf.vs", "brdf.fs");
	Shader backgroundShader("background.vs", "background.fs");


	Shader shadowShader("shadow.vs", "shadow.fs");



	Shader glass("glass.vs", "glass.fs");





	pbrShader.use();
	pbrShader.setInt("irradianceMap", 0);
	pbrShader.setInt("prefilterMap", 1);
	pbrShader.setInt("brdfLUT", 2);
	pbrShader.setInt("albedoMap", 3);
	pbrShader.setInt("normalMap", 4);
	pbrShader.setInt("metallicMap", 5);
	pbrShader.setInt("roughnessMap", 6);
	pbrShader.setInt("aoMap", 7);
	pbrShader.setInt("shadowMap", 8);
	backgroundShader.use();
	backgroundShader.setInteger("environmentMap", 0);


	glass.use();


	glm::vec3 lightPositions[] = {
	   glm::vec3(-10.0f,  10.0f, 10.0f),
	   glm::vec3(10.0f,  10.0f, 10.0f),
	   glm::vec3(-10.0f, -10.0f, 10.0f),
	   glm::vec3(10.0f, -10.0f, 10.0f),
	};
	glm::vec3 lightColors[] = {
		glm::vec3(10.0f, 10.0f, 12.0f),
		glm::vec3(10.0f, 10.0f, 12.0f),
		glm::vec3(10.0f, 10.0f, 12.0f),
		glm::vec3(10.0f, 10.0f, 12.0f)
	};



	glm::mat4 captureViews[] =
	{
		glm::lookAt(glm::vec3(0.0f, 0.0f, 0.0f), glm::vec3(1.0f,  0.0f,  0.0f), glm::vec3(0.0f, -1.0f,  0.0f)),
		glm::lookAt(glm::vec3(0.0f, 0.0f, 0.0f), glm::vec3(-1.0f,  0.0f,  0.0f), glm::vec3(0.0f, -1.0f,  0.0f)),
		glm::lookAt(glm::vec3(0.0f, 0.0f, 0.0f), glm::vec3(0.0f,  1.0f,  0.0f), glm::vec3(0.0f,  0.0f,  1.0f)),
		glm::lookAt(glm::vec3(0.0f, 0.0f, 0.0f), glm::vec3(0.0f, -1.0f,  0.0f), glm::vec3(0.0f,  0.0f, -1.0f)),
		glm::lookAt(glm::vec3(0.0f, 0.0f, 0.0f), glm::vec3(0.0f,  0.0f,  1.0f), glm::vec3(0.0f, -1.0f,  0.0f)),
		glm::lookAt(glm::vec3(0.0f, 0.0f, 0.0f), glm::vec3(0.0f,  0.0f, -1.0f), glm::vec3(0.0f, -1.0f,  0.0f))
	};



	// pbr: setup framebuffer
	// ----------------------
	unsigned int captureFBO;
	unsigned int captureRBO;
	glGenFramebuffers(1, &captureFBO);
	glGenRenderbuffers(1, &captureRBO);

	glBindFramebuffer(GL_FRAMEBUFFER, captureFBO);
	glBindRenderbuffer(GL_RENDERBUFFER, captureRBO);
	glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT24, width, height);
	glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, captureRBO);




	char c_path[] = "Ressources/objets/outside/out2.obj";
	char blackb_path[] = "Ressources/objets/blackboard/blacboard.obj";
	char table_path[] = "Ressources/objets/table/table.obj";
	char Street_Props_path[] = "Ressources/objets/locker/locker.obj";

	char sp_path[] = "Ressources/objets/mushroom/LOD2.obj";
	char door_p[] = "Ressources/objets/door/door.obj";

	//pirate
	char body_path[] = "Ressources/objets/pirate/pirate.obj";

	char debris_path[] = "Ressources/objets/debris/debris.obj";
	char food_path[] = "Ressources/objets/food/food.obj";


	GLuint cubeMapTexture;
	glGenTextures(1, &cubeMapTexture);
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_CUBE_MAP, cubeMapTexture);

	// texture parameters
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

	std::string pathToCubeMap = "Ressources/cubemaps/fjaderholmarna/";

	std::map<std::string, GLenum> facesToLoad = {
			{pathToCubeMap + "posx.jpg",GL_TEXTURE_CUBE_MAP_POSITIVE_X},
			{pathToCubeMap + "posy.jpg",GL_TEXTURE_CUBE_MAP_POSITIVE_Y},
			{pathToCubeMap + "posz.jpg",GL_TEXTURE_CUBE_MAP_POSITIVE_Z},
			{pathToCubeMap + "negx.jpg",GL_TEXTURE_CUBE_MAP_NEGATIVE_X},
			{pathToCubeMap + "negy.jpg",GL_TEXTURE_CUBE_MAP_NEGATIVE_Y},
			{pathToCubeMap + "negz.jpg",GL_TEXTURE_CUBE_MAP_NEGATIVE_Z},
		};
	
	for (std::pair<std::string, GLenum> pair : facesToLoad) {
		loadCubemapFace(pair.first.c_str(), pair.second);
	}



	GLuint cubeMapTexture2;
	glGenTextures(1, &cubeMapTexture2);
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_CUBE_MAP, cubeMapTexture2);

	// texture parameters
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	pathToCubeMap = "Ressources/cubemaps/ssr/";

	facesToLoad = {
		{pathToCubeMap + "posx.jpg",GL_TEXTURE_CUBE_MAP_POSITIVE_X},
			{pathToCubeMap + "posy.jpg",GL_TEXTURE_CUBE_MAP_POSITIVE_Y},
			{pathToCubeMap + "posz.jpg",GL_TEXTURE_CUBE_MAP_POSITIVE_Z},
			{pathToCubeMap + "negx.jpg",GL_TEXTURE_CUBE_MAP_NEGATIVE_X},
			{pathToCubeMap + "negy.jpg",GL_TEXTURE_CUBE_MAP_NEGATIVE_Y},
			{pathToCubeMap + "negz.jpg",GL_TEXTURE_CUBE_MAP_NEGATIVE_Z},
	};
	//load the six faces
	for (std::pair<std::string, GLenum> pair : facesToLoad) {
		loadCubemapFace(pair.first.c_str(), pair.second);
	}


	//CREATE OBJECTS

	Object blackboard(blackb_path);
	blackboard.makeObject2(pbrShader, false);
	blackboard.translate(glm::vec3(0.0, 0.0, 9.1f));
	blackboard.translate(glm::vec3(0.0, 1.0, 0.0));
	blackboard.loadTex("Ressources/objets/blackboard/albedo.jpg", "Ressources/objets/blackboard/normal.png", "Ressources/objets/blackboard/roughness.png", "Ressources/objets/blackboard/ao.png", "Ressources/objets/blackboard/metallic.png");






	Object outside(c_path);
	outside.makeObject2(pbrShader, false);
	outside.loadTex("Ressources/objets/outside/out_albedo.png", "Ressources/objets/outside/out_norm.png", "Ressources/objets/outside/out_roughness.png", "Ressources/objets/outside/out_ao.png", "Ressources/objets/default/default.png");


	Object table(table_path);
	table.makeObject2(pbrShader, false);
	table.scale(glm::vec3(1.5f));
	table.rotate(glm::vec3(0.0, 1.0, 0.0), 3.14 / 2);
	table.translate(glm::vec3(0.0, 0.0, 2.0));
	table.translate(glm::vec3(-2.0, 0.0, 0.0));
	table.loadTex("Ressources/objets/table/albedo.jpg", "Ressources/objets/table/normal.jpg", "Ressources/objets/table/roughness.jpg", "Ressources/objets/default/default.png", "Ressources/objets/default/default.png");




	Object streetP(Street_Props_path);
	streetP.makeObject2(pbrShader, false);
	streetP.scale(glm::vec3(1.8));
	streetP.translate(glm::vec3(5, 0.0, -5));
	streetP.loadTex("Ressources/objets/locker/albedo.jpg", "Ressources/objets/locker/normal.jpg", "Ressources/objets/locker/roughness.jpg",
		"Ressources/objets/default/default.png", "Ressources/objets/default/default.png");


	Object sphere(sp_path);
	sphere.makeObject2(pbrShader);
	//sphere.scale(glm::vec3(100.0));
	sphere.loadTex("Ressources/objets/mushroom/albedo.jpg", "Ressources/objets/mushroom/normal.jpg", "Ressources/objets/mushroom/roughness.jpg", "Ressources/objets/default/default.png", "Ressources/objets/default/default.png");

	Object door(door_p);
	door.makeObject2(pbrShader);
	door.translate(glm::vec3(0.0, -1.0, 0.0));
	door.loadTex("Ressources/objets/door/albedo.jpg", "Ressources/objets/door/normal.jpg", "Ressources/objets/door/roughness.jpg", "Ressources/objets/default/default.png", "Ressources/objets/door/metalness.jpg");

	Object body(body_path);
	body.makeObject(glass);

	

	Object hardware("Ressources/objets/hardware/hardware.obj");
	hardware.makeObject2(pbrShader);
	hardware.scale(glm::vec3(2));
	hardware.translate(glm::vec3(0.0, 0.0, -4.8));

	hardware.loadTex("Ressources/objets/hardware/albedo.jpg", "Ressources/objets/hardware/normal.jpg", "Ressources/objets/hardware/roughness.jpg", "Ressources/objets/default/default.png", "Ressources/objets/default/default.png");

	Object debris(debris_path);
	debris.makeObject2(pbrShader);
	debris.loadTex("Ressources/objets/debris/albedo.jpg", "Ressources/objets/debris/normal.jpg", "Ressources/objets/debris/roughness.jpg", "Ressources/objets/default/default.png", "Ressources/objets/default/default.png");


	Object wind("Ressources/objets/window/window.obj");
	wind.makeObject2(pbrShader);
	//bris.translate(glm::vec3(0.832, -9.44, 2.67));
	wind.loadTex("Ressources/objets/window/albedo.jpg", "Ressources/objets/window/normal.jpg", "Ressources/objets/window/roughness.jpg", "Ressources/objets/default/default.png", "Ressources/objets/default/default.png");

	Object furniture("Ressources/objets/furniture/furniture.obj");
	furniture.makeObject2(pbrShader);
	furniture.translate(glm::vec3(-8, 0, 8));
	furniture.loadTex("Ressources/objets/furniture/albedo.jpg", "Ressources/objets/furniture/normal.jpg", "Ressources/objets/furniture/roughness.jpg",
		"Ressources/objets/default/default.png", "Ressources/objets/default/default.png");

	Object storage("Ressources/objets/storage1/storage1.obj");
	storage.makeObject2(pbrShader);
	storage.loadTex("Ressources/objets/storage1/albedo.jpg", "Ressources/objets/storage1/normal.jpg", "Ressources/objets/storage1/roughness.jpg", "Ressources/objets/default/default.png", "Ressources/objets/default/default.png");
	
	Object cardboard1("Ressources/objets/cardboard1/cardboard1.obj");
	cardboard1.makeObject2(pbrShader);
	cardboard1.loadTex("Ressources/objets/cardboard1/albedo.jpg", "Ressources/objets/cardboard1/normal.jpg", "Ressources/objets/cardboard1/roughness.jpg", "Ressources/objets/default/default.png", "Ressources/objets/default/default.png");

	Object cardboard2("Ressources/objets/cardboard2/cardboard2.obj");
	cardboard2.makeObject2(pbrShader);
	cardboard2.loadTex("Ressources/objets/cardboard2/albedo.jpg", "Ressources/objets/cardboard2/normal.jpg", "Ressources/objets/cardboard2/roughness.jpg", "Ressources/objets/default/default.png", "Ressources/objets/default/default.png");


	Object trash("Ressources/objets/trash/trash.obj");
	trash.makeObject2(pbrShader);
	trash.loadTex("Ressources/objets/trash/albedo.jpg", "Ressources/objets/trash/normal.jpg", "Ressources/objets/trash/roughness.jpg", "Ressources/objets/default/default.png", "Ressources/objets/default/default.png");

	double prev = 0;
	int deltaFrame = 0;
	//fps function
	auto fps = [&](double now) {
		double deltaTime = now - prev;
		deltaFrame++;
		if (deltaTime > 0.5) {
			prev = now;
			const double fpsCount = (double)deltaFrame / deltaTime;
			deltaFrame = 0;
			std::cout << "\r FPS: " << fpsCount;
			std::cout.flush();
		}
	};




	glm::mat4 view = camera.GetViewMatrix();
	glm::mat4 perspective = camera.GetProjectionMatrix();




	// pbr: create an irradiance cubemap, and re-scale capture FBO to irradiance scale.
	// --------------------------------------------------------------------------------
	unsigned int irradianceMap;
	glGenTextures(1, &irradianceMap);
	glBindTexture(GL_TEXTURE_CUBE_MAP, irradianceMap);
	for (unsigned int i = 0; i < 6; ++i)
	{
		glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X + i, 0, GL_RGB16F, 32, 32, 0, GL_RGB, GL_FLOAT, nullptr);
	}
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

	glBindFramebuffer(GL_FRAMEBUFFER, captureFBO);
	glBindRenderbuffer(GL_RENDERBUFFER, captureRBO);
	glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT24, 32, 32);

	// pbr: solve diffuse integral by convolution to create an irradiance (cube)map.
	// -----------------------------------------------------------------------------
	irradianceShader.use();
	irradianceShader.setInteger("environmentMap", 0);
	irradianceShader.setMat4("projection", perspective);
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_CUBE_MAP, cubeMapTexture);

	glViewport(0, 0, 32, 32); // don't forget to configure the viewport to the capture dimensions.
	glBindFramebuffer(GL_FRAMEBUFFER, captureFBO);
	for (unsigned int i = 0; i < 6; ++i)
	{
		irradianceShader.setMat4("view", captureViews[i]);
		glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_CUBE_MAP_POSITIVE_X + i, irradianceMap, 0);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		renderCube();
	}
	glBindFramebuffer(GL_FRAMEBUFFER, 0);

	// pbr: create a pre-filter cubemap, and re-scale capture FBO to pre-filter scale.
	// --------------------------------------------------------------------------------
	unsigned int prefilterMap;
	glGenTextures(1, &prefilterMap);
	glBindTexture(GL_TEXTURE_CUBE_MAP, prefilterMap);
	for (unsigned int i = 0; i < 6; ++i)
	{
		glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X + i, 0, GL_RGB16F, 128, 128, 0, GL_RGB, GL_FLOAT, nullptr);
	}
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR); // be sure to set minification filter to mip_linear 
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	// generate mipmaps for the cubemap so OpenGL automatically allocates the required memory.
	glGenerateMipmap(GL_TEXTURE_CUBE_MAP);

	// pbr: run a quasi monte-carlo simulation on the environment lighting to create a prefilter (cube)map.
	// ----------------------------------------------------------------------------------------------------
	prefilterShader.use();
	prefilterShader.setInteger("environmentMap", 0);
	prefilterShader.setMat4("projection", perspective);
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_CUBE_MAP, cubeMapTexture);

	glBindFramebuffer(GL_FRAMEBUFFER, captureFBO);
	unsigned int maxMipLevels = 5;
	for (unsigned int mip = 0; mip < maxMipLevels; ++mip)
	{
		// reisze framebuffer according to mip-level size.
		unsigned int mipWidth = static_cast<unsigned int>(128 * std::pow(0.5, mip));
		unsigned int mipHeight = static_cast<unsigned int>(128 * std::pow(0.5, mip));
		glBindRenderbuffer(GL_RENDERBUFFER, captureRBO);
		glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT24, mipWidth, mipHeight);
		glViewport(0, 0, mipWidth, mipHeight);

		float roughness = (float)mip / (float)(maxMipLevels - 1);
		prefilterShader.setFloat("roughness", roughness);
		for (unsigned int i = 0; i < 6; ++i)
		{
			prefilterShader.setMat4("view", captureViews[i]);
			glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_CUBE_MAP_POSITIVE_X + i, prefilterMap, mip);

			glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
			renderCube();
		}
	}
	glBindFramebuffer(GL_FRAMEBUFFER, 0);

	// pbr: generate a 2D LUT from the BRDF equations used.
	// ----------------------------------------------------
	unsigned int brdfLUTTexture;
	glGenTextures(1, &brdfLUTTexture);

	// pre-allocate enough memory for the LUT texture.
	glBindTexture(GL_TEXTURE_2D, brdfLUTTexture);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RG16F, 512, 512, 0, GL_RG, GL_FLOAT, 0);
	// be sure to set wrapping mode to GL_CLAMP_TO_EDGE
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

	// then re-configure capture framebuffer object and render screen-space quad with BRDF shader.
	glBindFramebuffer(GL_FRAMEBUFFER, captureFBO);
	glBindRenderbuffer(GL_RENDERBUFFER, captureRBO);
	glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT24, 512, 512);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, brdfLUTTexture, 0);

	glViewport(0, 0, 512, 512);
	brdfShader.use();
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	renderQuad();

	glBindFramebuffer(GL_FRAMEBUFFER, 0);


	unsigned int shadowMapFBO;
	glGenFramebuffers(1, &shadowMapFBO);

	unsigned int shadowMapWidth = 4092, shadowMapHeight = 4092;
	unsigned int shadowMap;
	glGenTextures(1, &shadowMap);
	glBindTexture(GL_TEXTURE_2D, shadowMap);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT, shadowMapWidth, shadowMapHeight, 0, GL_DEPTH_COMPONENT, GL_FLOAT, NULL);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_BORDER);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_BORDER);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	float clampColor[] = { 1.0f, 1.0f, 1.0f, 1.0f };
	glTexParameterfv(GL_TEXTURE_2D, GL_TEXTURE_BORDER_COLOR, clampColor);

	glBindFramebuffer(GL_FRAMEBUFFER, shadowMapFBO);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, shadowMap, 0);
	glDrawBuffer(GL_NONE);
	glReadBuffer(GL_NONE);
	glBindFramebuffer(GL_FRAMEBUFFER, 0);






	// initialize static shader uniforms before rendering
	// --------------------------------------------------

	pbrShader.use();
	pbrShader.setMat4("projection", perspective);

	backgroundShader.use();
	backgroundShader.setMat4("projection", perspective);

	// then before rendering, configure the viewport to the original framebuffer's screen dimensions
	int scrWidth, scrHeight;
	glfwGetFramebufferSize(window, &scrWidth, &scrHeight);
	glViewport(0, 0, scrWidth, scrHeight);




	unsigned int amount = 80;
	glm::mat4* modelMatrices;
	modelMatrices = new glm::mat4[amount];
	srand(static_cast<unsigned int>(glfwGetTime())); // initialize random seed
	float radius = 10.0;
	float offset = 2.5f;
	for (unsigned int i = 0; i < amount; i++)
	{
		glm::mat4 model = glm::mat4(1.0f);
		// 1. translation: displace along circle with 'radius' in range [-offset, offset]
		float angle = (float)i / (float)amount * 360.0f;
		float displacement = (rand() % (int)(2 * offset * 100)) / 100.0f - offset;
		float x = sin(angle) * radius + displacement;
		displacement = (rand() % (int)(2 * offset * 100)) / 100.0f - offset;
		float y = displacement * 0.4f; // keep height of asteroid field smaller compared to width of x and z
		displacement = (rand() % (int)(2 * offset * 100)) / 100.0f - offset;
		float z = cos(angle) * radius + displacement;
		model = glm::translate(model, glm::vec3(x, 0, z));


		float scale = static_cast<float>((rand() % 20) / 2000.0 + 0.05);
		model = glm::scale(model, glm::vec3(scale));

		// 3. rotation: add random rotation around a (semi)randomly picked rotation axis vector
		float rotAngle = static_cast<float>((rand() % 360));
		model = glm::rotate(model, rotAngle, glm::vec3(0.0f, 1.0f, 0.0f));

		// 4. now add to list of matrices
		modelMatrices[i] = model;
	}
	//


		/**********************************


		///PRINCIPAL WHILE LOOP*************************

		*/

	glm::vec3 pos_door = door.model * glm::vec4(door.positions[0], 1.0);
	//std::cout << glm::to_string(pos_door) << std::endl;




	glm::vec3 lightPos[4] = {
		glm::vec3(2.0f, 5.0f, 1.2f),
		glm::vec3(8.0f, 5.0f, -8.0f),
		glm::vec3(-7.0f, 5.0f, 3.0f),
		glm::vec3(-2.0f, 5.0f,-1.2f),
	};


	int count = 0;

	glfwSwapInterval(1);
	while (!glfwWindowShouldClose(window)) {




		processInput(window);


		view = camera.GetViewMatrix();
		glfwPollEvents();
		double now = glfwGetTime();



		glm::mat4 lightProjection, lightView;
		glm::mat4 lightSpaceMatrix;
		float near_plane = 1.0f, far_plane = 10;

		//lightProjection = glm::perspective(glm::radians(45.0f), (GLfloat)shadowMapWidth / (GLfloat)shadowMapHeight, near_plane, far_plane); // note that if you use a perspective projection matrix you'll have to change the light position as the current light position isn't enough to reflect the whole scene
		lightProjection = glm::ortho(-12.0f, 12.0f, -12.0f, 12.0f, near_plane, far_plane);
		lightView = glm::lookAt(lightPos[0], glm::vec3(0.0f), glm::vec3(0.0, 1.0, 0.0));
		lightSpaceMatrix = lightProjection * lightView;
		// render scene from light's point of view


		glEnable(GL_DEPTH_TEST);
		glViewport(0, 0, shadowMapWidth, shadowMapHeight);
		glBindFramebuffer(GL_FRAMEBUFFER, shadowMapFBO);
		glClear(GL_DEPTH_BUFFER_BIT);
		shadowShader.use();
		shadowShader.setMat4("lightProjection", lightSpaceMatrix);
		//shadowShader.setMat4("model", outside.model);
		//outside.Draw(shadowShader);

		shadowShader.setMat4("model", table.model);
		table.Draw(shadowShader);

		shadowShader.setMat4("model", blackboard.model);
		blackboard.Draw(shadowShader);
		shadowShader.setMat4("model", streetP.model);
		streetP.Draw(shadowShader);
		shadowShader.setMat4("model", trash.model);
		trash.Draw(shadowShader);

		glm::vec3 upd = door.model * glm::vec4(door.positions[0], 1.0);

		if (camera.Position.x > 1.0 && camera.Position.x< 4.0 && camera.Position.z > -8.0 && camera.Position.z < -6.0) {
			//std::cout << "campos";
			double t = now;
			if (std::abs(upd.x - pos_door.x) < 5.0) {
				//std::cout << glm::to_string(upd);
				//std::cout.flush();
				door.translate(glm::vec3(-.1, 0.0, 0.0));

			}
			//
		}



		//renderScene(shadowShader);
		//glBindFramebuffer(GL_FRAMEBUFFER, 0);
	


		// Switch back to the default framebuffer
		glBindFramebuffer(GL_FRAMEBUFFER, 0);
		// Switch back to the default viewport
		glViewport(0, 0, width, height);
		// Bind the custom framebuffer
		// Specify the color of the background



		glClearColor(0.5f, 0.5f, 0.5f, 1.0f);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		glEnable(GL_DEPTH_TEST);


		//instance.use();
		//instance.setMat4("view", view);
		//instance.setVector3f("camPos", camera.Position);
		//instance.setMat4("projection", perspective);



		pbrShader.use();
		glm::mat4 model = glm::mat4(1.0f);
		glm::mat4 view = camera.GetViewMatrix();
		pbrShader.setMat4("view", view);
		pbrShader.setVector3f("camPos", camera.Position);
		pbrShader.setVec3("lightPositions[0]", lightPos[0]);
		pbrShader.setVec3("lightPositions[1]", lightPos[1]);
		pbrShader.setVec3("lightPositions[2]", lightPos[2]);
		pbrShader.setVec3("lightPositions[3]", lightPos[3]);
		pbrShader.setVec3("lightColors[0]", lightColors[0]);
		pbrShader.setVec3("lightColors[1]", lightColors[1]);
		pbrShader.setVec3("lightColors[2]", lightColors[2]);
		pbrShader.setVec3("lightColors[3]", lightColors[3]);
		pbrShader.setMat4("lightSpaceMatrix", lightSpaceMatrix);
		// bind pre-computed IBL data
		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_CUBE_MAP, irradianceMap);
		glActiveTexture(GL_TEXTURE1);
		glBindTexture(GL_TEXTURE_CUBE_MAP, prefilterMap);
		glActiveTexture(GL_TEXTURE2);
		glBindTexture(GL_TEXTURE_2D, brdfLUTTexture);
		glActiveTexture(GL_TEXTURE8);
		glBindTexture(GL_TEXTURE_2D, shadowMap);


		glDepthFunc(GL_LEQUAL);
		outside.Draw(pbrShader);
		for (unsigned int i = 0; i < amount; i++)
		{
			sphere.Draw(pbrShader, modelMatrices[i]);
		}
		blackboard.Draw(pbrShader);
		table.Draw(pbrShader);
		streetP.Draw(pbrShader);
		door.Draw(pbrShader);

		hardware.Draw(pbrShader);

		debris.Draw(pbrShader);
		wind.Draw(pbrShader);
		furniture.Draw(pbrShader);
		storage.Draw(pbrShader);
		cardboard1.Draw(pbrShader);
		cardboard2.Draw(pbrShader);
		trash.Draw(pbrShader);




		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_CUBE_MAP, cubeMapTexture2);
		glass.use();
		glass.setMatrix4("V", view);
		glass.setMatrix4("P", perspective);
		glass.setVector3f("u_view_pos", camera.Position);

		glass.setMatrix4("M", body.model);
		glass.setMatrix4("itM", body.inverseModel);
		body.draw();

		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_CUBE_MAP, cubeMapTexture);
		//glBindTexture(GL_TEXTURE_CUBE_MAP, irradianceMap); // display irradiance map
		//glBindTexture(GL_TEXTURE_CUBE_MAP, prefilterMap); // display prefilter map
		backgroundShader.use();
		backgroundShader.setMat4("view", view);
		renderCube();
		glDepthFunc(GL_LESS);



		glfwSwapBuffers(window);
	}

	//clean up ressource
	glfwDestroyWindow(window);
	glfwTerminate();

	return 0;
}


void processInput(GLFWwindow* window) {

	double no;

	if (glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS)
		glfwSetWindowShouldClose(window, true);

	if (glfwGetKey(window, GLFW_KEY_A) == GLFW_PRESS)
		camera.ProcessKeyboardMovement(LEFT, 0.1);
	if (glfwGetKey(window, GLFW_KEY_D) == GLFW_PRESS)
		camera.ProcessKeyboardMovement(RIGHT, 0.1);

	if (glfwGetKey(window, GLFW_KEY_W) == GLFW_PRESS)
		camera.ProcessKeyboardMovement(FORWARD, 0.1);
	if (glfwGetKey(window, GLFW_KEY_S) == GLFW_PRESS)
		camera.ProcessKeyboardMovement(BACKWARD, 0.1);

	if (glfwGetKey(window, GLFW_KEY_RIGHT) == GLFW_PRESS)
		camera.ProcessKeyboardRotation(1, 0.0, 1);
	if (glfwGetKey(window, GLFW_KEY_LEFT) == GLFW_PRESS)
		camera.ProcessKeyboardRotation(-1, 0.0, 1);

	if (glfwGetKey(window, GLFW_KEY_UP) == GLFW_PRESS) {
		camera.ProcessKeyboardRotation(0.0, 1.0, 0.1);
		camera.now1 = glfwGetTime();
		//camera.ProcessKeyboardRotation(0.0, -1.0, .00001);
		camera.up_b = true;

	}

	if (glfwGetKey(window, GLFW_KEY_DOWN) == GLFW_PRESS)
		camera.ProcessKeyboardRotation(0.0, -1.0, 1);


}







void loadCubemapFace(const char* path, const GLenum& targetFace)
{
	int imWidth, imHeight, imNrChannels;
	unsigned char* data = stbi_load(path, &imWidth, &imHeight, &imNrChannels, 0);
	if (data)
	{

		glTexImage2D(targetFace, 0, GL_RGB, imWidth, imHeight, 0, GL_RGB, GL_UNSIGNED_BYTE, data);
		//glGenerateMipmap(targetFace);
	}
	else {
		std::cout << "Failed to Load texture" << std::endl;
		const char* reason = stbi_failure_reason();
		std::cout << reason << std::endl;
	}
	stbi_image_free(data);
}





unsigned int cubeVAO = 0;
unsigned int cubeVBO = 0;
void renderCube()
{
	// initialize (if necessary)
	if (cubeVAO == 0)
	{
		float vertices[] = {
			// back face
			-1.0f, -1.0f, -1.0f,  0.0f,  0.0f, -1.0f, 0.0f, 0.0f, // bottom-left
			 1.0f,  1.0f, -1.0f,  0.0f,  0.0f, -1.0f, 1.0f, 1.0f, // top-right
			 1.0f, -1.0f, -1.0f,  0.0f,  0.0f, -1.0f, 1.0f, 0.0f, // bottom-right         
			 1.0f,  1.0f, -1.0f,  0.0f,  0.0f, -1.0f, 1.0f, 1.0f, // top-right
			-1.0f, -1.0f, -1.0f,  0.0f,  0.0f, -1.0f, 0.0f, 0.0f, // bottom-left
			-1.0f,  1.0f, -1.0f,  0.0f,  0.0f, -1.0f, 0.0f, 1.0f, // top-left
			// front face
			-1.0f, -1.0f,  1.0f,  0.0f,  0.0f,  1.0f, 0.0f, 0.0f, // bottom-left
			 1.0f, -1.0f,  1.0f,  0.0f,  0.0f,  1.0f, 1.0f, 0.0f, // bottom-right
			 1.0f,  1.0f,  1.0f,  0.0f,  0.0f,  1.0f, 1.0f, 1.0f, // top-right
			 1.0f,  1.0f,  1.0f,  0.0f,  0.0f,  1.0f, 1.0f, 1.0f, // top-right
			-1.0f,  1.0f,  1.0f,  0.0f,  0.0f,  1.0f, 0.0f, 1.0f, // top-left
			-1.0f, -1.0f,  1.0f,  0.0f,  0.0f,  1.0f, 0.0f, 0.0f, // bottom-left
			// left face
			-1.0f,  1.0f,  1.0f, -1.0f,  0.0f,  0.0f, 1.0f, 0.0f, // top-right
			-1.0f,  1.0f, -1.0f, -1.0f,  0.0f,  0.0f, 1.0f, 1.0f, // top-left
			-1.0f, -1.0f, -1.0f, -1.0f,  0.0f,  0.0f, 0.0f, 1.0f, // bottom-left
			-1.0f, -1.0f, -1.0f, -1.0f,  0.0f,  0.0f, 0.0f, 1.0f, // bottom-left
			-1.0f, -1.0f,  1.0f, -1.0f,  0.0f,  0.0f, 0.0f, 0.0f, // bottom-right
			-1.0f,  1.0f,  1.0f, -1.0f,  0.0f,  0.0f, 1.0f, 0.0f, // top-right
			// right face
			 1.0f,  1.0f,  1.0f,  1.0f,  0.0f,  0.0f, 1.0f, 0.0f, // top-left
			 1.0f, -1.0f, -1.0f,  1.0f,  0.0f,  0.0f, 0.0f, 1.0f, // bottom-right
			 1.0f,  1.0f, -1.0f,  1.0f,  0.0f,  0.0f, 1.0f, 1.0f, // top-right         
			 1.0f, -1.0f, -1.0f,  1.0f,  0.0f,  0.0f, 0.0f, 1.0f, // bottom-right
			 1.0f,  1.0f,  1.0f,  1.0f,  0.0f,  0.0f, 1.0f, 0.0f, // top-left
			 1.0f, -1.0f,  1.0f,  1.0f,  0.0f,  0.0f, 0.0f, 0.0f, // bottom-left     
			// bottom face
			-1.0f, -1.0f, -1.0f,  0.0f, -1.0f,  0.0f, 0.0f, 1.0f, // top-right
			 1.0f, -1.0f, -1.0f,  0.0f, -1.0f,  0.0f, 1.0f, 1.0f, // top-left
			 1.0f, -1.0f,  1.0f,  0.0f, -1.0f,  0.0f, 1.0f, 0.0f, // bottom-left
			 1.0f, -1.0f,  1.0f,  0.0f, -1.0f,  0.0f, 1.0f, 0.0f, // bottom-left
			-1.0f, -1.0f,  1.0f,  0.0f, -1.0f,  0.0f, 0.0f, 0.0f, // bottom-right
			-1.0f, -1.0f, -1.0f,  0.0f, -1.0f,  0.0f, 0.0f, 1.0f, // top-right
			// top face
			-1.0f,  1.0f, -1.0f,  0.0f,  1.0f,  0.0f, 0.0f, 1.0f, // top-left
			 1.0f,  1.0f , 1.0f,  0.0f,  1.0f,  0.0f, 1.0f, 0.0f, // bottom-right
			 1.0f,  1.0f, -1.0f,  0.0f,  1.0f,  0.0f, 1.0f, 1.0f, // top-right     
			 1.0f,  1.0f,  1.0f,  0.0f,  1.0f,  0.0f, 1.0f, 0.0f, // bottom-right
			-1.0f,  1.0f, -1.0f,  0.0f,  1.0f,  0.0f, 0.0f, 1.0f, // top-left
			-1.0f,  1.0f,  1.0f,  0.0f,  1.0f,  0.0f, 0.0f, 0.0f  // bottom-left        
		};
		glGenVertexArrays(1, &cubeVAO);
		glGenBuffers(1, &cubeVBO);
		// fill buffer
		glBindBuffer(GL_ARRAY_BUFFER, cubeVBO);
		glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);
		// link vertex attributes
		glBindVertexArray(cubeVAO);
		glEnableVertexAttribArray(0);
		glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(float), (void*)0);
		glEnableVertexAttribArray(1);
		glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(float), (void*)(3 * sizeof(float)));
		glEnableVertexAttribArray(2);
		glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, 8 * sizeof(float), (void*)(6 * sizeof(float)));
		glBindBuffer(GL_ARRAY_BUFFER, 0);
		glBindVertexArray(0);
	}
	// render Cube
	glBindVertexArray(cubeVAO);
	glDrawArrays(GL_TRIANGLES, 0, 36);
	glBindVertexArray(0);
}

unsigned int quadVAO = 0;
unsigned int quadVBO;
void renderQuad()
{
	if (quadVAO == 0)
	{
		float quadVertices[] = {
			// positions        // texture Coords
			-1.0f,  1.0f, 0.0f, 0.0f, 1.0f,
			-1.0f, -1.0f, 0.0f, 0.0f, 0.0f,
			 1.0f,  1.0f, 0.0f, 1.0f, 1.0f,
			 1.0f, -1.0f, 0.0f, 1.0f, 0.0f,
		};
		// setup plane VAO
		glGenVertexArrays(1, &quadVAO);
		glGenBuffers(1, &quadVBO);
		glBindVertexArray(quadVAO);
		glBindBuffer(GL_ARRAY_BUFFER, quadVBO);
		glBufferData(GL_ARRAY_BUFFER, sizeof(quadVertices), &quadVertices, GL_STATIC_DRAW);
		glEnableVertexAttribArray(0);
		glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 5 * sizeof(float), (void*)0);
		glEnableVertexAttribArray(1);
		glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 5 * sizeof(float), (void*)(3 * sizeof(float)));
	}
	glBindVertexArray(quadVAO);
	glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
	glBindVertexArray(0);
}



