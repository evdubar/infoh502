#ifndef TEXTURE_H
#define TEXTURE_H




unsigned int loadTexture(char const* path);
class Textures {
public:
	unsigned int Albedo;
	unsigned int Normal;
	unsigned int Roughness;
	unsigned int Ao;
	unsigned int Metalness;
	


	Textures(char const* albedo, char const* normal, char const* rougness, char const* ao, char const* metalness) {
		if (albedo != "")
			this->Albedo = loadTexture(albedo);
		if (normal != "")
			this->Normal = loadTexture(normal);
		if (rougness != "")
			this->Roughness = loadTexture(rougness);
		if (ao != "")
			this->Ao = loadTexture(ao);
		if (metalness != "")
			this->Metalness = loadTexture(metalness);

	}



	Textures() {

	}





};

unsigned int loadTexture(char const* path)
{
	unsigned int textureID;
	glGenTextures(1, &textureID);

	int width, height, nrComponents;
	stbi_set_flip_vertically_on_load(true);
	unsigned char* data = stbi_load(path, &width, &height, &nrComponents, 0);
	if (data)
	{
		GLenum format;
		if (nrComponents == 1)
			format = GL_RED;
		else if (nrComponents == 3)
			format = GL_RGB;
		else if (nrComponents == 4)
			format = GL_RGBA;

		glBindTexture(GL_TEXTURE_2D, textureID);
		glTexImage2D(GL_TEXTURE_2D, 0, format, width, height, 0, format, GL_UNSIGNED_BYTE, data);
		glGenerateMipmap(GL_TEXTURE_2D);

		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

		stbi_image_free(data);
	}

	else
	{
		std::cout << "Texture failed to load at path: " << path << std::endl;
		stbi_image_free(data);
	}
	stbi_set_flip_vertically_on_load(false);
	return textureID;
}

#endif // !TEXTURE_CLASS_H
